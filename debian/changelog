python-arpy (1.1.1-6) UNRELEASED; urgency=medium

  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 06 Jan 2023 11:42:31 -0000

python-arpy (1.1.1-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 22:21:39 -0400

python-arpy (1.1.1-4) unstable; urgency=medium

  * Team upload.
  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Drop Python 2 support.
  * Set PYBUILD_NAME and run tests using pybuild.
  * Enable autopkgtest-pkg-python testsuite.
  * Bump debhelper compat level to 12.
  * d/copyright: Add header and fix syntax.
  * Use debhelper-compat instead of debian/compat.
  * Bump standards version to 4.4.0 (no changes).
  * Remove versioned B-D on python3, python3-all is enough.

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

 -- Ondřej Nový <onovy@debian.org>  Thu, 08 Aug 2019 13:12:09 +0200

python-arpy (1.1.1-3) unstable; urgency=low

  * Team upload.

  [ Christoph Egger ]
  * Add VCS-* headers

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ Scott Kitterman ]
  * Correct substitution variable for python3 interpreter depends (Closes:
    #867418)
  * Remove unneeded python:Provides
  * Update homepage for move to github
  * Add debian/watch

 -- Scott Kitterman <scott@kitterman.com>  Sun, 09 Jul 2017 01:38:51 -0400

python-arpy (1.1.1-2) unstable; urgency=low

  * Add python3 support

 -- Christoph Egger <christoph@debian.org>  Sat, 06 Jul 2013 15:02:46 +0200

python-arpy (1.1.1-1) unstable; urgency=low

  * Initial release. (Closes: #704594)

 -- Christoph Egger <christoph@debian.org>  Mon, 18 Mar 2013 17:15:12 +0100
